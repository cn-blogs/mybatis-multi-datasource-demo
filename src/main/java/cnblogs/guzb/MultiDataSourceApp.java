package cnblogs.guzb;

import cnblogs.guzb.biz1.Biz1Mapper;
import cnblogs.guzb.biz1.UserEntity;
import cnblogs.guzb.biz2.AuthorityEntity;
import cnblogs.guzb.biz2.Biz2Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class MultiDataSourceApp implements CommandLineRunner {
    @Autowired
    Biz1Mapper biz1Mapper;

    @Autowired
    Biz2Mapper biz2Mapper;

    public static void main(String[] args) {
        SpringApplication.run(MultiDataSourceApp.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        StringBuilder output1 = new StringBuilder();

        output1.append("第一个数据源(主数据源)的数据查询结果：\n");
        List<UserEntity> users = biz1Mapper.listAll();
        users.forEach(user -> output1.append("\t").append(user).append("\n"));
        System.out.println(output1);

        StringBuilder output2 = new StringBuilder();
        output2.append("第二个数据源(副数据源)的数据查询结果：\n");
        List<AuthorityEntity> authorities = biz2Mapper.listAll();
        authorities.forEach(authority -> output2.append("\t").append(authority).append("\n"));

        Integer authorityCount = biz2Mapper.totalCount();
        output2.append("\n\t").append("总条数为：").append(authorityCount);

        System.out.println(output2);
    }
}