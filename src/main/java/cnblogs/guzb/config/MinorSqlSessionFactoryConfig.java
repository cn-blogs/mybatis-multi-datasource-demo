package cnblogs.guzb.config;

import org.apache.ibatis.datasource.unpooled.UnpooledDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;

/**
 * 副数据源配置, 查询的数据库为 biz2.sqlite3
 */
@Configuration
@MapperScan(
        basePackages = {"cnblogs.guzb.biz2"},
        sqlSessionFactoryRef = "MinorSqlSessionFactory"
)
public class MinorSqlSessionFactoryConfig {

    @Bean(name = "MinorDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.minor")
    public DataSource getPrimaryDateSource() {
        // 这个MyBatis内置的无池化的数据源，仅仅用于演示，实际工程请更换成具体的数据源对象
        return new UnpooledDataSource();
    }

    @Bean(name = "MinorSqlSessionFactory")
    public SqlSessionFactory primarySqlSessionFactory(
            @Qualifier("MinorDataSource") DataSource datasource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(datasource);
        // 主数据源的XML SQL配置资源
        Resource[] xmlMapperResources = new PathMatchingResourcePatternResolver().getResources("classpath:mappers/minor/*.xml");
        bean.setMapperLocations(xmlMapperResources);
        return bean.getObject();
    }


    @Bean("MinorSqlSessionTemplate")
    public SqlSessionTemplate primarySqlSessionTemplate(
            @Qualifier("MinorSqlSessionFactory") SqlSessionFactory sessionFactory) {
        return new SqlSessionTemplate(sessionFactory);
    }


}
