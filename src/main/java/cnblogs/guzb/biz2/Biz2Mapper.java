package cnblogs.guzb.biz2;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface Biz2Mapper {

    @Select("select * from authority")
    List<AuthorityEntity> listAll();

    Integer totalCount();

}
