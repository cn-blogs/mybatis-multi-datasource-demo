package cnblogs.guzb.biz1;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface Biz1Mapper {

    @Select("select * from user")
    List<UserEntity> listAll();

    UserEntity getById(@Param("id") Long id);
}
