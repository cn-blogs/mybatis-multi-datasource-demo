# 手动装配多个MyBatis SqlSessionFactory （多数据源）

本示例工程，使用了两个 sqlite3 数据库用作演示，分别是 biz1.sqlite3 和 biz2.sqlite3, 均位于 resource目录下。
两个数据库，各有一张表，其数据如下：

- biz1.sqlite3 中的 user 表

  | id  | username | password | name | mobile_phone |
  | --- |----------|----------|------| ------------- |
  | 1  | scallion | codefate15218 | 葱葱 | 18866568848|
  | 1  | lotus    | codefate15218 | 莲珍 | 15243436869|

- biz2.sqlite3 中的 authority 表

  | name      | title   | type          | 
  |-----------|---------|---------------|
  | USER      | 一般用户    | ROLE | 葱葱 | 
  | AMDIN     | 系统管理员   | ROLE | 莲珍 | 
  | SALE_PART | 销售部     | ORGANIZE | 莲珍 | 

> :pushpin: 你可以到[官网](https://www.sqlite.org/index.html)下载桌面客户端工具，来打开操作 sqlite 数据库。
